package wasteoftime;

import org.apache.commons.lang.StringUtils;
import org.apache.xmlbeans.xml.stream.CharacterData;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This is just scratched out code. No quality control here. It is used to translate english to
 * either pirate, leet or pig latin
 */
public class Translate {

    private static StringBuilder sb = new StringBuilder();

    /**
     * Translates from English to Pig Latin
     *
     * @param in The plain string
     * @return The translated
     */
    public static String pigLatin(String in){

        if(in.length() == 0){return in;}

        String[] words = in.split("[^\\p{L}]+");
        String[] punctuation  = in.split("[\\p{L}]+");

        int word_count = words.length;
        int punctuation_count = punctuation.length;

        int word_cursor = 0;
        int punctuation_cursor = 0;

        if( punctuation[0].equals("")){
            punctuation_cursor++;
        }

        if(words[0].equals("")){
            word_cursor++;
            sb.append(punctuation[punctuation_cursor++]);
        }

        while(word_cursor < word_count && punctuation_cursor < punctuation_count){

            sb.append(wordToPigLatin(words[word_cursor]));
            sb.append(punctuation[punctuation_cursor]);

            word_cursor++;
            punctuation_cursor++;

        }

        while(word_cursor < word_count){
            sb.append(wordToPigLatin(words[word_cursor]));
            word_cursor++;
        }
        while(punctuation_cursor < punctuation_count){
            sb.append(punctuation[punctuation_cursor]);
            punctuation_cursor++;
        }

        return  sb.toString();
    }

    private static String wordToPigLatin(String word){

        StringBuilder sb = new StringBuilder();

        String suffix = "ay";

        if(word.length() == 0){
            return "";
        }
        if(word.length() == 1){
            return word + suffix;
        }

        char firstChar = word.charAt(0);
        boolean capitalized = false;
        if(Character.isUpperCase(firstChar)){
            capitalized = true;
            firstChar = Character.toLowerCase(firstChar);
        }

        for(int i = 1; i < word.length(); i++){

            char c = word.charAt(i);
            if(i == 1 && capitalized ){
                c = Character.toUpperCase(c);
            }
            sb.append(c);
        }

        sb.append(firstChar);
        sb.append(suffix);

        return sb.toString();
    }


    /**
     * Translates from english to Pirate speak
     * @param in The english string.
     * @return The pirate string.
     */
    public static String pirate(String in){


        Map<String,String> dictionary = new HashMap<String,String>();
        dictionary.put("are", "be");
        dictionary.put("your", "yerr");
        dictionary.put("you","yee");
        dictionary.put("drink", "grog");
        dictionary.put("the","thar");
        dictionary.put("car","land-boat");
        dictionary.put("truck","tugboat");
        dictionary.put("food","slop");
        dictionary.put("cash","booty");
        dictionary.put("money","gold");
        dictionary.put("parrot","first mate");
        dictionary.put("street","canal");
        dictionary.put("GPS","starry night");
        dictionary.put("engine","sails");
        dictionary.put("energy","gusty winds");
        dictionary.put("internet","carrier pigeon network");
        dictionary.put("light","moonlight");
        dictionary.put("fun","hacking and slashing");
        dictionary.put("fighting","sword fighting");
        dictionary.put("talking","swashbuckling");
        dictionary.put("vegetable","mush");
        dictionary.put("mad","salty");
        dictionary.put("crazy","mad");
        dictionary.put("sick","queasy");
        dictionary.put("man","land-lubber");
        dictionary.put("punishment","plank walking");
        dictionary.put("ball","cannonball");
        dictionary.put("hello","ahoy");
        dictionary.put("drive","sail");
        dictionary.put("run","swim");
        dictionary.put("walk","float");
        dictionary.put("animal","fish");
        dictionary.put("kitchen","galley");
        dictionary.put("jail","the brig");
        dictionary.put("yes","aye");
        dictionary.put("no","nah");
        dictionary.put("liquor","poison");


        String patternString = "("+ StringUtils.join(dictionary.keySet(), "|") +")";
        Pattern pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(in);

        StringBuffer sb = new StringBuffer();
        while(matcher.find()) {
            String found = matcher.group(1);
            String replacement = dictionary.get(matcher.group(1).toLowerCase());
            if(Character.isUpperCase(found.charAt(0))){
                replacement = replacement.replaceFirst("^"+replacement.charAt(0),""+Character.toUpperCase(replacement.charAt(0)));
            }

            matcher.appendReplacement(sb, replacement);
        }
        matcher.appendTail(sb);

        sb.append(" Yarrr!");

        return sb.toString();
    }

    /**
     * Translates from english to 1337
     * @param in The english string
     * @return The 1337 57|^1|\|&
     */
    public static String leet(String in){


        Map<Character,String> dictionary = new HashMap<Character,String>();
        dictionary.put('a', "4");
        dictionary.put('b', "6");
        dictionary.put('c', "(");
        dictionary.put('d', "|)");
        dictionary.put('e', "3");
        dictionary.put('f', "|=");
        dictionary.put('g', "&");
        dictionary.put('h', "|-|");
        dictionary.put('i', "!");
        dictionary.put('j', "_/");
        dictionary.put('k', "|<");
        dictionary.put('l', "1");
        dictionary.put('m', "/`\\");
        dictionary.put('n', "|\\|");
        dictionary.put('o', "0");
        dictionary.put('p', "|*");
        dictionary.put('q', "9");
        dictionary.put('r', "|^");
        dictionary.put('s', "5");
        dictionary.put('t', "7");
        dictionary.put('u', "(_)");
        dictionary.put('v', "\\/");
        dictionary.put('w', "\\^/");
        dictionary.put('x', "><");
        dictionary.put('y', "`/");
        dictionary.put('z', "2");


        StringBuffer sb = new StringBuffer();

        int length = in.length();

        for(int i = 0;i < length; i++){
            String replace = dictionary.get(in.charAt(i));
            if(replace == null){
                replace = ""+in.charAt(i);
            }
            sb.append(replace);
        }

        return sb.toString();
    }



}

